import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;
import javax.naming.*;
import java.io.*;
 
public class SendMessage
{
	private Message message;
	
	public SendMessage(String uname, String pword)
	{
		final String username = uname;
		final String password = pword;
		
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
				
		Session session = Session.getInstance(props, new javax.mail.Authenticator() 
		{
			protected PasswordAuthentication getPasswordAuthentication() 
			{
				return new PasswordAuthentication(username, password);
			}
		});
		message = new MimeMessage(session);
	}//constructor
	
	public void addRecipient(String recipient) throws AddressException, MessagingException
	{
		message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient));
	}
	
	public void clearRecipients() throws MessagingException
	{
		message.setRecipients(Message.RecipientType.TO,null);
	}
	
	public void setMessage(String messageText) throws MessagingException
	{
		message.setText(messageText);
	}
	
	public void setFrom(String from) throws MessagingException, UnsupportedEncodingException
	{
		Address address = new InternetAddress(from,from);
		message.setFrom(address);
	}
	
	public void setSubject(String subject)throws MessagingException
	{
		message.setSubject(subject);
	}
	
	public void send() throws MessagingException
	{
		Transport.send(message);
	}

}