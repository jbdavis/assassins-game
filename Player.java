import java.util.*;
import java.io.*;
import javax.mail.*;
import javax.mail.internet.*;

public class Player
{
	private final int DENIED_CONFIRM = 0;
	private final int IMMUNE = 1;
	private String emailAddress;
	private String emailHQ;
	private String passwordHQ;
	private SendMessage messageOut;
	private ReadMessage messageIn;
	private String playerName;
	private int rank;
	private int score;
	private int messagesStored;
	private ArrayList<Player> hunters = new ArrayList<Player>();
	private Player target = null;
	private boolean waitingForConfirm = false;
	private boolean immune = false;
	private ArrayList<Player> immuneTo = new ArrayList<Player>();
	
	public Player(String address, String name, String usernameHQ, String passwordHQ)
	{
		this.emailAddress = address;
		this.playerName = name;
		this.emailHQ = usernameHQ;
		this.passwordHQ = passwordHQ;
		
		rank = 1;
		score = 0;
		messagesStored = 0;
	}
	
	public void addHunter(Player player)
	{
		hunters.add(player);
	}

	public ArrayList<Player> getHunters()
	{
		return hunters;
	}
	
	public void removeHunter(Player player)
	{
		for(int i=0;i<hunters.size();i++)
		{
			if(hunters.get(i).equals(player))
			{
				hunters.remove(i);
			}
		}
	}
	
	public void setTarget(Player player)
	{
		target = player;
		try
		{
			messageOut = new SendMessage(emailHQ,passwordHQ);
			messageOut.setSubject("New Target");
			messageOut.setMessage("Your new target is: "+player.getName());
			messageOut.clearRecipients();
			messageOut.addRecipient(emailAddress);
			messageOut.send();
		}
		catch(AddressException ae)
		{
			ae.printStackTrace();
		}
		catch(MessagingException me)
		{
			me.printStackTrace();
		}
	}
	
	public Player getTarget()
	{
		return target;
	}
	
	public String getAddress()
	{
		return emailAddress;
	}
	
	public String getName()
	{
		return this.playerName;
	}
	
	public int getRank()
	{
		return rank;
	}
	
	public int getScore()
	{
		return score;
	}

	public void readMessages(String m)
	{
		String message = m.toLowerCase().trim();
		if(message.contains("done"))
			{
				//System.out.println("done recieved.");
				if(!target.isImmuneTo(this))
				{
					
					killConfirm();
				}
				else
					this.noKill(IMMUNE,this);
			}
			else if(message.contains("yes"))
			{
				for(int i=0;i<hunters.size();i++)
				{
					//System.out.println("yes recieved.");
					if(hunters.get(i).isWaitingForConfirm())
					{
						reassign(hunters.get(i));
						hunters.get(i).noLongerWaiting();
					}
				}
			}
			else if(message.contains("no"))
			{
				for(int i=0;i<hunters.size();i++)
				{
					//System.out.println("no recieved.");
					if(hunters.get(i).isWaitingForConfirm())
					{
						this.noKill(DENIED_CONFIRM,hunters.get(i));
					}
				}
			}
			else
				System.out.println("dump: "+message);
	}
	
	public void killConfirm()
	{
		try
		{
			messageOut = new SendMessage(emailHQ,passwordHQ);
			messageOut.setSubject("Kill Confirmation");
			messageOut.setMessage("Did "+this.playerName+" kill you?");
			messageOut.clearRecipients();
			messageOut.addRecipient(target.getAddress());
			messageOut.send();
		}
		catch(AddressException ae)
		{
			ae.printStackTrace();
		}
		catch(MessagingException me)
		{
			me.printStackTrace();
		}
		waitingForConfirm = true;
	}
	
	public boolean isWaitingForConfirm()
	{
		return waitingForConfirm;
	}
	
	public void noLongerWaiting()
	{
		waitingForConfirm = false;
	}
	
	public void justDied()
	{
		this.setImmuneTo(null);
	}
	
	public boolean isImmuneTo(Player player)
	{
		if(immune)
		{
			if(immuneTo.isEmpty() || immuneTo.contains(player))
				return true;
		}
		return false;
	}
	
	public void setImmuneTo(Player player)
	{
		immune = true;
		if(player!=null)
		{
			immuneTo.add(player);
			int bonus = player.getRank() - this.rank;
			if(bonus > 0)
				score += 1+bonus;
			else
				score ++;
			rank++;
			this.immuneCountdown(this,player);
		}
		else
		{
			if(rank>0)
				rank--;
			immuneTo.clear();
			this.immuneCountdown(this,player);
		}
		
	}
	
	public void immuneCountdown(Player player, Player protectedFrom)
	{
		final Player timerPlayer = player;
		final Player proPlayer = protectedFrom;
		final java.util.Timer timer = new java.util.Timer();
		TimerTask safeTimer = new TimerTask()
		{
			@Override
			public void run()
			{
				timerPlayer.noLongerImmune(proPlayer);
				timer.cancel();
				timer.purge();
			}	
		};
		timer.schedule(safeTimer,1800000,1000);
	}
	
	public void noLongerImmune(Player protectedFrom)
	{
		immune = false;
		if(protectedFrom!=null)
			immuneTo.remove(protectedFrom);
		else
			immuneTo.clear();
	}
	
	public void noKill(int code, Player messagedPlayer)
	{
		try
		{
			if(code == DENIED_CONFIRM)
			{
				messageOut = new SendMessage(emailHQ,passwordHQ);
				messageOut.setSubject("Kill Confirmation");
				messageOut.setMessage("Your kill was not confirmed to have happened.");
			}
			else if(code == IMMUNE)
			{
				messageOut = new SendMessage(emailHQ,passwordHQ);
				messageOut.setSubject("Kill Denied");
				messageOut.setMessage("Your target either died or killed you within the last 30 minutes.");
			}
			messageOut.clearRecipients();
			messageOut.addRecipient(messagedPlayer.getAddress());
			messageOut.send();
			messagedPlayer.noLongerWaiting();
		}
		catch(AddressException ae)
		{
			ae.printStackTrace();
		}
		catch(MessagingException me)
		{
			me.printStackTrace();
		}
	}
	
	public void reassign(Player player)
	{
		try
		{
			messageOut = new SendMessage(emailHQ,passwordHQ);
			messageOut.setMessage("Reassign "+player.getAddress());
			messageOut.clearRecipients();
			messageOut.addRecipient(emailHQ);
			messageOut.send();
		}
		catch(AddressException ae)
		{
			ae.printStackTrace();
		}
		catch(MessagingException me)
		{
			me.printStackTrace();
		}
	}

	public String toString()
	{
		if(!this.immuneTo.isEmpty())
		{
			String immuneList = "";
			for(int i=0;i<immuneTo.size();i++)
			{
				immuneList=immuneList.concat(" "+immuneTo.get(i).getName());
			}
			return (this.playerName+" -> "+this.target.getName()+" "+this.rank+" "+this.score+" "+this.immune+" "+immuneList);
		}
		else
			return (this.playerName+" -> "+this.target.getName()+" "+this.rank+" "+this.score+" "+this.immune+" "+"death immunity");
	}
}