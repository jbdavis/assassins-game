import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.net.*;
import java.util.*;
import java.io.*;
import javax.swing.border.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.naming.*;

public class AssassinMain extends JFrame
{
	private static AssassinMain client;
	private ArrayList<Player> player = new ArrayList<Player>();
	private NewEmailThread emailThread;
	private TimerTask countTimeUp;
	private java.util.Timer secondsTracker;
	
	final File playerFile= new File("player.txt");
	
	private String username;
	private String password;
	
	private SendMessage sMessage;
	private ReadMessage rMessage;
	
	private JPanel loginPanel;
	private JPanel mainPanel;
	private JPanel addPlayerPanel;
	
	private int days,hours,minutes,seconds;
	private String timeString,daysString;
	final JLabel timePassed = new JLabel(timeString);
	final JLabel daysPassed = new JLabel(daysString);
	
	private boolean initialized = false;
	
	
	private void loginInitUI()
	{
		timePassed.setVisible(false);
		daysPassed.setVisible(false);
		loginPanel=new JPanel();
		GridBagLayout gridBag = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints();	
		
		JButton loginButton = new JButton("Login");
		JLabel userNameLabel = new JLabel("Email Address:");
		JLabel passwordLabel = new JLabel("Password:");
		
		final JTextField userNameBox = new JTextField();
		final JPasswordField passwordBox = new JPasswordField();
		
		loginPanel.setLayout(gridBag);
	
		gbc.weightx = 1;
		gbc.gridx = 0;
		gbc.gridy = 0;
		loginPanel.add(userNameLabel,gbc);
		
		gbc = new GridBagConstraints();
		gbc.fill=GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1;
		gbc.gridx = 1;
		gbc.gridy = 0;
		loginPanel.add(userNameBox,gbc);
		
		gbc = new GridBagConstraints();
		gbc.weightx = 1;
		gbc.gridx = 0;
		gbc.gridy = 1;
		loginPanel.add(passwordLabel,gbc);
		
		gbc = new GridBagConstraints();
		gbc.fill=GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1;
		gbc.gridx = 1;
		gbc.gridy = 1;
		loginPanel.add(passwordBox,gbc);
		
		gbc = new GridBagConstraints();
		gbc.weightx = 1;
		gbc.gridx = 1;
		gbc.gridy = 2;
		loginPanel.add(loginButton,gbc);		
				
		loginButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent event)
			{
				if(!userNameBox.getText().trim().isEmpty())
				{
					if(!new String(passwordBox.getPassword()).trim().isEmpty())
					{
						username = userNameBox.getText().toString().trim();
						password = new String(passwordBox.getPassword()).trim();
						sMessage = new SendMessage(username,password);
						rMessage = new ReadMessage(username,password);
										
						loginPanel.setVisible(false);
						mainInitUI();
					}
					else
						JOptionPane.showMessageDialog(null,"Enter a password.","Login Error",JOptionPane.ERROR_MESSAGE);
				}
				else
					JOptionPane.showMessageDialog(null,"Enter an email address.","Login Error",JOptionPane.ERROR_MESSAGE);
			}
		});
		
		getContentPane().add(loginPanel,BorderLayout.CENTER);
		getRootPane().setDefaultButton(loginButton);
	}//loginInitUI
	
	private void mainInitUI()
	{
		timeString = "00:00:00";
		daysString = "0 Days";
		mainPanel = new JPanel();
		player.clear();
		
		GridBagLayout gridBag = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints();	
		
		final JButton addPlayerButton = new JButton("Add Player");
		final JButton initButton = new JButton("Initialize");
		
		timePassed.setHorizontalAlignment(JLabel.CENTER);
		timePassed.setVerticalAlignment(JLabel.CENTER);
		timePassed.setFont(new Font("DS-Digital",Font.PLAIN,52));
		//timePassed.setForeground(Color.RED);
		
		daysPassed.setHorizontalAlignment(JLabel.CENTER);
		daysPassed.setVerticalAlignment(JLabel.CENTER);
		daysPassed.setFont(new Font("Dialog",Font.PLAIN,36));
		
		secondsTracker = new java.util.Timer();
		countTimeUp = new TimerTask()
		{
			@Override
			public void run()
			{
				if(seconds%10 == 0)
				{
					try
					{
						FileWriter debugOut = new FileWriter(new File("debug.txt"),false);
						debugOut.write("");
						debugOut.close();
						debugOut = new FileWriter(new File("debug.txt"),true);
						for(int i=0;i<player.size();i++)
						{
							debugOut.write(player.get(i).toString()+" "+System.getProperty("line.separator"));
						}
						debugOut.close();
					}
					catch(IOException ioe)
					{
						ioe.printStackTrace();
					}
				}
				if(seconds<59)
					seconds++;
				else if(seconds==59)
				{
					seconds=0;
					if(minutes<59)
						minutes++;
					else if(minutes==59)
					{
						minutes=0;
						if(hours<23)
							hours++;
						else if(hours==23)
						{
							hours=0;
							days++;
						}
					}
				}
				if(hours<10)
				{
					if(minutes<10)
					{
						if(seconds<10)
							timeString = "0"+hours+":0"+minutes+":0"+seconds;
						else
							timeString = "0"+hours+":0"+minutes+":"+seconds;
					}
					else
					{
						if(seconds<10)
							timeString = "0"+hours+":"+minutes+":0"+seconds;
						else
							timeString = "0"+hours+":"+minutes+":"+seconds;
					}
				}
				else
				{
					if(minutes<10)
					{
						if(seconds<10)
							timeString = ""+hours+":0"+minutes+":0"+seconds;
						else
							timeString = ""+hours+":0"+minutes+":"+seconds;
					}
					else
					{
						if(seconds<10)
							timeString = ""+hours+":"+minutes+":0"+seconds;
						else
							timeString = ""+hours+":"+minutes+":"+seconds;
					}
				}
				
				if(days == 1)
					daysString = ""+days+" Day";
				else
					daysString = ""+days+" Days";
				timePassed.setText(timeString);
				daysPassed.setText(daysString);
			}	
		};
		
		if(playerFile.isFile())
		{
			Scanner playerScan = null;
			try
			{
				playerScan = new Scanner(playerFile);
			}
			catch(FileNotFoundException fnfe)
			{
				fnfe.printStackTrace();
			}
			while(playerScan.hasNextLine())
			{
				String line = playerScan.nextLine();
				String[] lineSplit = line.split(" ");
				Player tempPlayer = new Player(lineSplit[0],lineSplit[1].replace("_"," "),username,password);
				player.add(tempPlayer);
			}
		}
		
		
		
		mainPanel.setLayout(gridBag);
	
		gbc.fill = GridBagConstraints.BOTH;
		gbc.insets = new Insets(0,0,50,0);
		gbc.gridx = 0;
		gbc.gridy = 0;
		mainPanel.add(addPlayerButton, gbc);
		gbc.gridy = 1;
		mainPanel.add(initButton, gbc);
		gbc.weightx=1;
		gbc.gridy = 2;
		mainPanel.add(daysPassed, gbc);
		gbc.gridy = 3;
		mainPanel.add(timePassed, gbc);
		
		addPlayerButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent event)
			{
				mainPanel.setVisible(false);
				addPlayerInitUI();
			}		
		});
		
		initButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent event)
			{
				addPlayerButton.setEnabled(false);
				initButton.setEnabled(false);
				addPlayerButton.setVisible(false);
				initButton.setVisible(false);
				initialize();
			}
		});
		
		getContentPane().add(mainPanel,BorderLayout.CENTER);		
	}
	
	public void addPlayerInitUI()
	{
		addPlayerPanel = new JPanel();
		GridBagLayout gridBag = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints();
		
		addPlayerPanel.setLayout(gridBag);
		
		final DefaultListModel<String> playerListModel = new DefaultListModel<String>();
		final JList<String> playerList = new JList<String>(playerListModel);
		JScrollPane playerPane = new JScrollPane(playerList);
		
		playerList.setLayoutOrientation(JList.VERTICAL);
		playerList.setCellRenderer(new JListRenderer());
		
		final DefaultComboBoxModel<String> providerModel = new DefaultComboBoxModel<String>();
		final JComboBox<String> providerComboBox = new JComboBox<String>(providerModel);
		ArrayList<String> providerNames = new ArrayList<String>();
		final ArrayList<String> providerEnding = new ArrayList<String>();
		JLabel providerLabel = new JLabel("Provider");
		
		final JTextField playerNameBox = new JTextField();
		final JTextField phoneNumberBox = new JTextField();
		JLabel playerNameLabel = new JLabel("Player Name");
		JLabel phoneNumberLabel = new JLabel("Phone Number");
		
		
		JButton addPlayerButton = new JButton("Add Player");
		final JButton deletePlayerButton = new JButton("Delete Player");
		JButton backButton = new JButton("Back");
		
		playerList.setLayoutOrientation(JList.VERTICAL);
		playerList.setCellRenderer(new JListRenderer());
		
		
		gbc.fill=GridBagConstraints.BOTH;
		gbc.anchor=GridBagConstraints.NORTH;
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.weighty=1;
		gbc.weightx=1;
		gbc.gridx=0;
		gbc.gridy=0;
		addPlayerPanel.add(playerPane,gbc);
		
		gbc=new GridBagConstraints();		
		gbc.fill=GridBagConstraints.BOTH;
		gbc.weightx=1;
		gbc.gridx=0;
		gbc.gridy=1;
		addPlayerPanel.add(playerNameLabel,gbc);
		gbc.gridx=1;
		addPlayerPanel.add(playerNameBox,gbc);
		gbc.gridx=2;
		addPlayerPanel.add(providerLabel,gbc);
		
		gbc.gridx=0;
		gbc.gridy=2;
		addPlayerPanel.add(phoneNumberLabel,gbc);
		gbc.gridx=1;
		addPlayerPanel.add(phoneNumberBox,gbc);
		gbc.gridx=2;
		addPlayerPanel.add(providerComboBox,gbc);
	
		gbc.gridx=0;
		gbc.gridy=3;
		addPlayerPanel.add(addPlayerButton,gbc);
		gbc.gridx=1;
		addPlayerPanel.add(deletePlayerButton,gbc);
		gbc.gridx=2;
		addPlayerPanel.add(backButton,gbc);
		
		File providerFile= new File("provider.txt");
		if(providerFile.isFile())
		{
			Scanner providerScan = null;
			try
			{
				providerScan = new Scanner(providerFile);
			}
			catch(FileNotFoundException fnfe)
			{
				fnfe.printStackTrace();
			}
			while(providerScan.hasNextLine())
			{
				String line = providerScan.nextLine();
				String[] lineSplit = line.split(" ");
				providerNames.add(lineSplit[0].replace("_"," "));
				providerEnding.add(lineSplit[1]);
			}
			for(int i=0;i<providerNames.size();i++)
			{
				providerModel.addElement(providerNames.get(i));
			}
		}
		
		for(int i=0;i<player.size();i++)
		{
			playerListModel.add(playerListModel.getSize(),"" + player.get(i).getName()+" "+player.get(i).getAddress());
			playerListModel.add(playerListModel.getSize(),"SEPARATOR");
		}
	
		backButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent event)
			{
				addPlayerPanel.setVisible(false);
				mainInitUI();
			}
		});
		
		addPlayerButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent event)
			{
				if(!playerNameBox.getText().trim().isEmpty())
				{
					if(!phoneNumberBox.getText().trim().isEmpty())
					{
						if(!providerComboBox.getSelectedItem().toString().trim().isEmpty())
						{
							String pName,pNum,prov;
							pName = playerNameBox.getText().trim();
							pNum = phoneNumberBox.getText().trim();
							prov = providerEnding.get(providerComboBox.getSelectedIndex()).trim();
							FileWriter playerFileWriter = null;
							Player tempPlayer = new Player(pNum.concat(prov),pName,username,password);
							player.add(tempPlayer);
							playerListModel.add(playerListModel.getSize(),"" + tempPlayer.getName()+" "+tempPlayer.getAddress());
							playerListModel.add(playerListModel.getSize(),"SEPARATOR");
							try
							{
								playerFileWriter = new FileWriter(playerFile,true);
								playerFileWriter.write(tempPlayer.getAddress()+" "+tempPlayer.getName().replace(" ","_")+System.getProperty("line.separator"));
								playerFileWriter.close();
							}
							catch(IOException ioe)
							{
								ioe.printStackTrace();
							}
							playerNameBox.setText("");
							phoneNumberBox.setText("");
							providerComboBox.setSelectedIndex(0);
						}
						else
							JOptionPane.showMessageDialog(null,"Choose a provider","Player Add Error",JOptionPane.ERROR_MESSAGE);
					}
					else
						JOptionPane.showMessageDialog(null,"Enter a phone number","Player Add Error",JOptionPane.ERROR_MESSAGE);
				}
				else
					JOptionPane.showMessageDialog(null,"Enter a player name","Player Add Error",JOptionPane.ERROR_MESSAGE);
			}
		});
		
		deletePlayerButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent event)
			{
				if(!playerList.isSelectionEmpty())
				{
					int selection = playerList.getSelectedIndex();
					if(selection%2!=0)
					{
						playerList.clearSelection();
						JOptionPane.showMessageDialog(null,"Select a player.","Removal Error",JOptionPane.ERROR_MESSAGE);
					}
					else
					{
						if(JOptionPane.showConfirmDialog(deletePlayerButton,"Delete this player?","Delete Confirmation",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE)==0)
						{
							player.remove(selection/2);
							playerListModel.remove(selection);
							playerListModel.remove(selection); //To remove separator as well.
							try
							{
								FileWriter clunkyRemover = new FileWriter(playerFile,false);
								for(int i=0;i<player.size();i++)
									clunkyRemover.write(player.get(i).getAddress()+" "+player.get(i).getName().replace(" ","_")+System.getProperty("line.separator"));
								clunkyRemover.close();
							}
							catch(IOException ioe)
							{
								ioe.printStackTrace();
							}
						}
					}
				}
				else
					JOptionPane.showMessageDialog(null,"Select a player.","Removal Error",JOptionPane.ERROR_MESSAGE);
			}
		});
				
		getContentPane().add(addPlayerPanel,BorderLayout.CENTER);
	}
	
	public void initialize()
	{
		initialized = true;
		player.get(0).setTarget(player.get(10));
		player.get(10).addHunter(player.get(0));
		
		player.get(1).setTarget(player.get(2));
		player.get(2).addHunter(player.get(1));
		
		player.get(2).setTarget(player.get(13));
		player.get(13).addHunter(player.get(2));
		
		player.get(3).setTarget(player.get(4));
		player.get(4).addHunter(player.get(3));
		
		player.get(4).setTarget(player.get(11));
		player.get(11).addHunter(player.get(4));
		
		player.get(5).setTarget(player.get(12));
		player.get(12).addHunter(player.get(5));
		
		player.get(6).setTarget(player.get(9));
		player.get(9).addHunter(player.get(6));
		
		player.get(7).setTarget(player.get(5));
		player.get(5).addHunter(player.get(7));
		
		player.get(8).setTarget(player.get(0));
		player.get(0).addHunter(player.get(8));
		
		player.get(9).setTarget(player.get(8));
		player.get(8).addHunter(player.get(9));
		
		player.get(10).setTarget(player.get(3));
		player.get(3).addHunter(player.get(10));
		
		player.get(11).setTarget(player.get(7));
		player.get(7).addHunter(player.get(11));
		
		player.get(12).setTarget(player.get(6));
		player.get(6).addHunter(player.get(12));
		
		player.get(13).setTarget(player.get(1));
		player.get(1).addHunter(player.get(13));
		
		for(int i=0;i<player.size();i++)
		{
			System.out.println(player.get(i).getName()+" -> "+player.get(i).getTarget().getName());
		}
		/*Random rand = new Random();
		int targetTry;
		boolean set = false;
		for(int i=0;i<player.size();i++)
		{
			if(i==player.size()-2)
			{
				if(player.get(player.size()-1).getHunters().isEmpty())
				{
					player.get(i).setTarget(player.get(player.size()-1));
					player.get(player.size()-1).addHunter(player.get(i));
					System.out.println("forced");
					System.out.println(player.get(i).getName()+" -> "+player.get(i).getTarget().getName());
					continue;
				}			
			}
			if(player.get(i).getTarget()==null)
			{
				while(!set)
				{
					targetTry=rand.nextInt(player.size());
					if(targetTry!=i)
					{
						if(player.get(targetTry).getHunters().isEmpty())
						{
							if(!player.get(i).getHunters().contains(player.get(targetTry)))
							{
								player.get(i).setTarget(player.get(targetTry));
								player.get(targetTry).addHunter(player.get(i));
								set=true;
							}
						}	
					}
				}
			}
			set=false;
			System.out.println(player.get(i).getName()+" -> "+player.get(i).getTarget().getName());
		}		
		*/
		timePassed.setVisible(true);
		daysPassed.setVisible(true);
		secondsTracker.scheduleAtFixedRate(countTimeUp,0,1000);
		try
		{
			emailThread = new NewEmailThread(player,username,password,client);
			emailThread.start();
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
	}
	
	public void reassign(String address)
	{
		Player tempPlayer=null,tempPlayerTarget=null;
		for(int i=0;i<player.size();i++)
		{
			if(player.get(i).getAddress().substring(0,10).equalsIgnoreCase(address) || "1".concat(player.get(i).getAddress().substring(0,10)).equalsIgnoreCase(address))
			{
				tempPlayer = player.get(i);
			}
		}
		tempPlayerTarget = tempPlayer.getTarget();
		tempPlayer.setImmuneTo(tempPlayerTarget);
		tempPlayerTarget.justDied();
		
		
		if((tempPlayer.getRank()<Math.floor(player.size()/2)+1) && tempPlayer.getRank() < 6)
		{	
			tempPlayerTarget.removeHunter(tempPlayer);
			tempPlayer.setTarget(tempPlayerTarget.getTarget());
			tempPlayerTarget.setTarget(tempPlayer);
			tempPlayer.getTarget().addHunter(tempPlayer);
		}
		else
		{
			ArrayList<Player> winner = new ArrayList<Player>();
			winner.add(tempPlayer);
			endTheGame(winner);
		}
	}
	
	public void endTheGame(ArrayList<Player> winner)
	{
		try
		{
			sMessage.setSubject("The hunt has ended");
			if(winner.size() == 1)
				sMessage.setMessage(""+winner.get(0).getName()+" is the Master Assassin, congratulations.");
			else
			{
				String winners = "";
				for(int i=0;i<winner.size();i++)
				{
					if(i == winner.size()-1)
						winners = winners.concat(", and "+winner.get(i).getName());
					else if(i == 0)
						winners = winners.concat(winner.get(i).getName());
					else
						winners = winners.concat(", "+winner.get(i).getName());
				}
				sMessage.setMessage(""+winners+" are the Master Assassins, congratulations.");
			}
			sMessage.clearRecipients();
			for(int i=0;i<player.size();i++)
			{
				sMessage.addRecipient(player.get(i).getAddress());
			}
			sMessage.send();
		}
		catch(AddressException ae)
		{
			ae.printStackTrace();
		}
		catch(MessagingException me)
		{
			me.printStackTrace();
		}
		
		try
		{
			FileWriter endGameWriter = new FileWriter(new File("endGameData.txt"),false);
			endGameWriter.write("");
			endGameWriter.close();
			endGameWriter = new FileWriter(new File("endGameData.txt"),true);
			for(int i=0;i<player.size();i++)
			{
				endGameWriter.write(player.get(i).getName()+" "+player.get(i).getScore()+" "+System.getProperty("line.separator"));
			}
			endGameWriter.write("Days passed: "+daysString+" "+"Time passed: "+timeString);
			endGameWriter.close();
			
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
		
		
		System.exit(0);
	}
	
	public AssassinMain()
	{
		loginInitUI();
		setTitle("Assassin Headquarters");
		setSize(500,500);
		setLocationRelativeTo(null);	
		addWindowListener(new WindowAdapter() 
		{
			@Override
			public void windowClosing(WindowEvent event)
			{
				if(!player.isEmpty() && initialized)
				{
					ArrayList<Player> winner = new ArrayList<Player>();
					int highScore = 0;
					for(int i=0;i<player.size();i++)
					{
						if(player.get(i).getScore()>highScore)
						{
							winner.clear();
							winner.add(player.get(i));
							highScore = player.get(i).getScore();
						}
						else if(player.get(i).getScore() == highScore)
						{
							winner.add(player.get(i));
						}
					}
					endTheGame(winner);
				}
			}		
		});
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args)
	{
		SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				client = new AssassinMain();
				client.setVisible(true);
			}
		});	
	}
}

class NewEmailThread extends Thread
{
	ArrayList<Player> playerArrayList;
	ReadMessage inboxReader;
	int messageCount;
	AssassinMain mainThread;
			
	public NewEmailThread(ArrayList<Player> playerArrayList,String username,String password, AssassinMain mainThread) throws IOException
	{
		this.playerArrayList = playerArrayList;
		this.mainThread = mainThread;
		inboxReader = new ReadMessage(username,password);	
		messageCount = 0;
	}

	@Override
	public void run()
	{
		while(true)
		{
			if(inboxReader.getMessageCount() > messageCount)
			{
				for(int i=messageCount+1;i<=inboxReader.getMessageCount();i++)
				{
					Message message = inboxReader.readMessage(i);
					try
					{
						String[] messArray = null;
						String passableMessage = "";
						Multipart tempMessMulti = null;
						String tempMessString = null;
						try
						{
							if(message.getContentType().contains("multipart"))
								tempMessMulti = (Multipart) message.getContent();
							else if(message.getContentType().contains("TEXT/PLAIN"))
								tempMessString = (String) message.getContent();

							if(tempMessMulti != null)
							{
								String contents = "";
								for(int j=0;j<tempMessMulti.getCount();j++)
								{
									BodyPart tempPart = tempMessMulti.getBodyPart(j);
									if(tempPart.getContentType().contains("TEXT/PLAIN"))
									{
										contents = (String)tempPart.getContent();
										int end = contents.indexOf('-');
										if(end>0)
											contents = contents.substring(0,end);
									}
								}
								passableMessage = contents;
							}
							else if(tempMessString !=null)
							{
								tempMessString = tempMessString.trim();
								messArray = tempMessString.split(" ");
								for(int j=0;j<messArray.length;j++)
								{
									if(messArray[j].contains("-"))
									{
										messArray = Arrays.copyOf(messArray,j-1);
										break;
									}
									else
										messArray[j] = messArray[j].trim();
								}
								for(int j=0;j<messArray.length;j++)
								{
									passableMessage = passableMessage.concat(messArray[j]);
								}
							}
						}
						catch(IOException ioe)
						{
							ioe.printStackTrace();
						}
						if(messArray!=null)
						{
							if(!messArray[0].equalsIgnoreCase("reassign"))
							{
								Address[] from = message.getFrom();
								for(int j=0;j<playerArrayList.size();j++)
								{
									if(playerArrayList.get(j).getAddress().substring(0,10).equals(from[0].toString().substring(0,10)) || "1".concat(playerArrayList.get(j).getAddress().substring(0,10)).equals(from[0].toString().substring(0,11)) )
										playerArrayList.get(j).readMessages(passableMessage);
										//System.out.println(from[0].toString()+" "+playerArrayList.get(j).getName()+" "+passableMessage);
								}
							}
							else
								mainThread.reassign(messArray[1].substring(0,10));
						}
						else
						{
							Address[] from = message.getFrom();
							for(int j=0;j<playerArrayList.size();j++)
							{
								if(playerArrayList.get(j).getAddress().substring(0,10).equals(from[0].toString().substring(0,10)) || "1".concat(playerArrayList.get(j).getAddress().substring(0,10)).equals(from[0].toString().substring(0,11)))
									playerArrayList.get(j).readMessages(passableMessage);
									//System.out.println(from[0].toString()+" "+playerArrayList.get(j).getName()+" "+passableMessage);
							}
						}
					}
					catch(MessagingException me)
					{
						me.printStackTrace();
					}
				}
				messageCount=inboxReader.getMessageCount();
			}
		}
	}
}

class JListRenderer extends JLabel implements ListCellRenderer<String>
{
	JSeparator separator;
	final String SEPARATOR = "SEPARATOR";
	
	public JListRenderer() 
	{
		setOpaque(true);
		setBorder(new EmptyBorder(1, 1, 1, 1));
		separator = new JSeparator(JSeparator.HORIZONTAL);
	}
	public Component getListCellRendererComponent(JList<? extends String> list, String value, int index, boolean isSelected, boolean cellHasFocus) 
	{
		String str = (value == null) ? "" : value.toString();
		if (SEPARATOR.equals(str)) 
			return separator;
		if (isSelected) 
		{
			setBackground(list.getSelectionBackground());
			setForeground(list.getSelectionForeground());
		} 
		else 
		{
			setBackground(list.getBackground());
			setForeground(list.getForeground());
		}
		setFont(list.getFont());
		setText(str);
		return this;
	}
}