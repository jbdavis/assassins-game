import java.util.*;
import javax.mail.*;
import java.io.*;
 
public class ReadMessage
{
	private String subject;
	private String messageText;
	private Message message;
	private Store store;
	private Folder inbox;
	private String userName;
	private String passWord;
	private Properties props;
	
	public ReadMessage(String uname, String pword)
	{
		userName = uname;
		passWord = pword;
		
		props = new Properties();
        props.setProperty("mail.store.protocol", "imaps");
		this.connect();
	}//constructor
	
	public void connect()
	{
		try
		{
			Session session = Session.getInstance(props, null);
			store = session.getStore();
			store.connect("imap.gmail.com",userName,passWord);
			inbox = store.getFolder("INBOX");
			inbox.open(Folder.READ_ONLY);
		}
		catch(NoSuchProviderException nspe)
		{
			nspe.printStackTrace();
		}
		catch(MessagingException me)
		{
			me.printStackTrace();
		}
	}
	
	public int getMessageCount()
	{
		try
		{
			return inbox.getMessageCount();
		}
		catch (FolderClosedException fce) 
		{
			fce.printStackTrace();
			this.connect();
			return this.getMessageCount();
        }
		catch(MessagingException me)
		{
			me.printStackTrace();
		}
		return -1;
	}
	
	public void close()
	{
		try
		{
			store.close();
		}
		catch(MessagingException me)
		{
			me.printStackTrace();
		}
	}
	
	public Message readMessage(int index)
	{
		try
		{
			message = inbox.getMessage(index);
			return message;//System.out.println(message.getContent());
		}
		catch(MessagingException me)
		{
			me.printStackTrace();
		}
		catch(IndexOutOfBoundsException ioobe)
		{
			ioobe.printStackTrace();
		}
		return null;
	}
}